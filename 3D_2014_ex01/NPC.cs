﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _3D_2014_ex01
{
    class NPC : CharacterSprite
    {

       public NPC(Image sheet, int width, int height)
        {
            SpriteSheet = sheet;
            SpriteWidth = width;
            SpriteHeight = height;
            ViewDirection = Direction.South;
            SpriteCursor = 0;
            SpriteSheetWidth = SpriteSheet.Width - SpriteWidth;
        }

       /// <summary>
       /// Reduces the NPC's life points by 1
       /// Does not check for collision ! 
       /// </summary>
        public override void Collide()
        {
            LifePoints--;
        }

        /// <summary>
        /// Which part of the spritesheet should be displayed
        /// </summary>
        /// <returns>Rectangle that contains information about position and size of single sprite in spritesheet</returns>
        public override Rectangle GetSourceSelectionRectangle()
        {
            return new Rectangle(SpriteCursor, 0, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Display-position and size of the CharacterSprite
        /// </summary>
        /// <returns>Rectangle that contains information about display-position and size</returns>
        public override Rectangle GetDestinationRectangle()
        {
            return new Rectangle(CurrentPositionX, CurrentPositionY, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Moves the CharacterSprite
        /// </summary>
        /// <param name="dir">execute move into this direction</param>
        public override void Move(Direction dir)
        {
            ViewDirection = dir;
            move();
        }

        /// <summary>
        /// Move in the previsouly set view direction
        /// </summary>
        public void move()
        {
            if (SpriteCursor < SpriteSheetWidth) { SpriteCursor += SpriteWidth; }
            else { SpriteCursor = 0; }

            switch (ViewDirection)
            {
                case Direction.North:
                    if (CurrentPositionY < 0) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    break;
                case Direction.East:
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = 0; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.South:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = 0; } else { CurrentPositionY += StepSize; }
                    break;
                case Direction.West:
                    if (CurrentPositionX < 0) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                case Direction.NorthEast:
                    if (CurrentPositionY < 0) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = 0; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.SouthEast:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = 0; } else { CurrentPositionY += StepSize; }
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = 0; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.SouthWest:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = 0; } else { CurrentPositionY += StepSize; }
                    if (CurrentPositionX < 0) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                case Direction.NorthWest:
                    if (CurrentPositionY < 0) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    if (CurrentPositionX < 0) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                default: break;
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="c"></param>
        public override void IsCollidingWith(CharacterSprite c)
        {
            throw new NotImplementedException();
        }
    }
}
