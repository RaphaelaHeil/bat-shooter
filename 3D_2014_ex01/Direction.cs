﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_2014_ex01
{
    /// <summary>
    /// Enumeration that represents the eight possible view directions.
    /// </summary>
    public enum Direction : int
    {
        South, // == 0 by default 
        SouthWest, // == 1 by default 
        West, // ... 
        NorthWest,
        North,
        NorthEast,
        East,
        SouthEast
    }
}
