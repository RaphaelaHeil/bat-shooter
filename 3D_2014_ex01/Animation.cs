﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace _3D_2014_ex01
{
    public partial class Animation : Form
    {

        private Player Player; //Sprite controlled by the human player
        HashSet<NPC> Bats = new HashSet<NPC>(); //currently active enemies/bats
        private Image Bar, Heart, DeadBat, Numbers, BatImage, SpeckImage, ProjectileImage; 
        
        private const int XMAX = 610; //area in which the player can walk
        private const int YMAX = 350;
        
        private int NumberDimension; //size of one (square)sprite in the number sprite sheet
        private int EnemyDimension;
        private int PlayerDimension;

        private int PlayerStartLP; //more than 12 are currently not supported by the lifebar-generator (i.e. images will oberlap)
        private int EnemyLP;
        
        private int MaxVisibleBats; //limit the number of active enemies
        private int BatKillObjective; //number of kills needed to win the game
       
        private bool up = false; //currently pressed buttons
        private bool down = false;
        private bool left = false;
        private bool right = false;
        private bool space = false;

         
        public Animation()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
        }

        ////////////////////// Implementations of Form Methods \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        private void Animation_Load(object sender, EventArgs e)
        {
            Setup(); //
            StartGame(); //set up the game 
        }

        private void Animation_Paint(object sender, PaintEventArgs e)
        {
            if (Player.KilledEnemies >= BatKillObjective) //check if enough enemies were killed
            {
                //show Win-Message
                String text = "Congratultions, you killed enough bats to keep your village save for another year!\nPlay again ?";
                if (MessageBox.Show(text, "You win", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    StartGame();
                }
                else
                {
                    this.Close();
                }

            }


            if (!Player.IsStillAlive()) //check if player is still alive
            {
                String text = "You were killed by a colony of bats. \nGame Over!\nPlay again ?";
                //show Lose-Message
                if (MessageBox.Show(text, "You lose", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    StartGame();
                }
                else
                {
                    this.Close();
                }
            }

            Random rand = new Random();
            if (Bats.Count == 0) //make sure there is always at least one bat
            {
                Bats.Add(GenerateBat(rand.Next(0, XMAX), rand.Next(0, YMAX)));
            }

            int NewBatChance = rand.Next(0, 9);
            if (NewBatChance == 6) // add another bat to the field with a 10% chance
            {
                if (Bats.Count < MaxVisibleBats)
                {
                    Bats.Add(GenerateBat(rand.Next(0, XMAX), rand.Next(0, YMAX)));
                }
            }

            //move all non-player objects:
            MoveBats();
            Player.MoveProjectiles();

            //move the player if applicable:
            if (up || down || left || right)
            {
                MoveInKeyDirection();
            }

            //check if the player is shooting:
            if (space)
            {
                Player.shoot();
            }

            //check for collisions before redrawing all elements (collisions may remove some elements)
            CheckForCollision();

            //always draw the player:
            e.Graphics.DrawImage(Player.SpriteSheet, Player.GetDestinationRectangle(), Player.GetSourceSelectionRectangle(), GraphicsUnit.Pixel);

            foreach (NPC bat in Bats) //draw all bats that are currently active: 
            {
                e.Graphics.DrawImage(bat.SpriteSheet, bat.GetDestinationRectangle(), bat.GetSourceSelectionRectangle(), GraphicsUnit.Pixel);
            }

            foreach (Projectile pro in Player.GetProjectiles()) //draw all projectiles that are currently active:
            {
                e.Graphics.DrawImage(pro.SpriteSheet, pro.GetDestinationRectangle(), pro.GetSourceSelectionRectangle(), GraphicsUnit.Pixel);
            }

            //redraw the LifePoint/KilledBat counter bar:
            ShowBar(e);

            System.Threading.Thread.Sleep(100); //pause for a while to reduce general speed of screen changes

            this.Invalidate(); //invalidate to ensure redrawing
        }

        private void Animation_KeyDown(object sender, KeyEventArgs e)
        {
            //check which key is pressed and set respective flags for movement handling later on 
            if (e.KeyCode == Keys.Up)
            {
                up = true;
            }
            if (e.KeyCode == Keys.Down)
            {
                down = true;
            }
            if (e.KeyCode == Keys.Left)
            {
                left = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                right = true;
            }
            if (e.KeyCode == Keys.Space)
            {
                space = true;
            }
        }

        private void Animation_KeyUp(object sender, KeyEventArgs e)
        {
            //check which key is released and set respective flags 
            if (e.KeyCode == Keys.Up)
            {
                up = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                down = false;
            }
            if (e.KeyCode == Keys.Left)
            {
                left = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                right = false;
            }
            if (e.KeyCode == Keys.Space)
            {
                space = false;
            }
        }


        //////////////////////////// Own Methods \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        /// <summary>
        /// Sets up basic game values, loads images, etc.
        /// </summary>
        /// <remarks>
        /// Could/Should be substituted by a method to read from a properties-file.
        /// </remarks>
        private void Setup()
        {
            NumberDimension = 8;
            PlayerDimension = 32;
            EnemyDimension = 24;
           
            PlayerStartLP = 10; //more than 12 are currently not supported by the lifebar-generator (i.e. images will overlap)
            EnemyLP = 1;
            
            MaxVisibleBats = 20;
            BatKillObjective = 15;
            
            //init other values by taste/need (e.g. stepsize, starting position, ...)

            //load images that are constantly needed for redrawing the interface: 
            Bar = _3D_2014_ex01.Properties.Resources.lifebar;
            Heart = _3D_2014_ex01.Properties.Resources.heart_big;
            DeadBat = _3D_2014_ex01.Properties.Resources.dead_bat;
            Numbers = _3D_2014_ex01.Properties.Resources.numbers; 
            BatImage = _3D_2014_ex01.Properties.Resources.bat;
            SpeckImage = _3D_2014_ex01.Properties.Resources.herr_von_speck_sheet;
            ProjectileImage = _3D_2014_ex01.Properties.Resources.bullet;
        }

        /// <summary>
        /// Prepares the form for a new game, i.e. resets values to default, clears old enemies, ...
        /// </summary>
        private void StartGame()
        {
            Bats.Clear();
            GeneratePlayer();
            InitBats();
            up = false; //reset currently pressed buttons since they sometimes seem to get stuck when showing the end-message box 
            down = false;
            left = false;
            right = false;
            space = false;
        }

        /// <summary>
        /// Adds a random amount (between 1 and 10) of bats to the game at startup.
        /// </summary>
        private void InitBats()
        {
            Random rand = new Random();
            int BatStartAmount = rand.Next(1, 10);

            for (int i = 0; i < BatStartAmount; i++)
            {
                Bats.Add(GenerateBat(rand.Next(0, XMAX), rand.Next(0, YMAX)));
            }
        }

        /// <summary>
        /// Creates a new NPC with a bat-sprite.
        /// </summary>
        /// <param name="x">Starting position x value</param>
        /// <param name="y">Starting position y value</param>
        /// <returns></returns>
        private NPC GenerateBat(int x, int y)
        {
            return new NPC(BatImage, EnemyDimension, EnemyDimension)
            {
                VerticalBound = YMAX,
                HorizontalBound = XMAX,
                CurrentPositionX = x,
                CurrentPositionY = y,
                StepSize = 4,
                AttackPoints = 1,
                LifePoints = EnemyLP
            };
        }

        /// <summary>
        /// Generates a new player character
        /// </summary>
        private void GeneratePlayer()
        {
            Player = new Player(SpeckImage, PlayerDimension, PlayerDimension)
            {
                VerticalBound = YMAX,
                HorizontalBound = XMAX,
                CurrentPositionX = 284,
                CurrentPositionY = 184,
                StepSize = 8,
                LifePoints = PlayerStartLP,
                AttackPoints = 1,
                Projectile = ProjectileImage
            };
        }

   
        /// <summary>
        /// Check if any of the currently active bats collide with the player or projectiles
        /// </summary>
        private void CheckForCollision()
        {
            foreach (NPC bat in Bats)
            {
                Player.IsCollidingWith(bat);
            }
            Bats.RemoveWhere(b => !b.IsStillAlive());
        }

        /// <summary>
        /// Moves the NPC-Bats by their basic step size, changes the view direction at a 10% chance.
        /// </summary>
        private void MoveBats()
        {
            Random rand = new Random();

            foreach (NPC bat in Bats)
            {
                int chance = rand.Next(0, 9);
                if (chance == 6) //change the view direction
                {
                    int dir = rand.Next(0, 7);
                    bat.Move((Direction)dir);
                }
                else
                {
                    bat.move(); //keep moving in the same direction
                }
            }
        }

        /// <summary>
        /// Draws the life-point/kill-count bar onto the form.
        /// </summary>
        /// <param name="e">PaintEvent</param>
        private void ShowBar(PaintEventArgs e)
        {
            e.Graphics.DrawImage(Bar, new Point(0,YMAX)); //draw the bar background

            int heartTop = YMAX + 27;
            for (int i = 0; i < Player.LifePoints; i++)
            {
                e.Graphics.DrawImage(Heart, new Point(25+(i*40), heartTop)); //add as many hearts as the player has LP left
            }

            int HorizontalPosition = XMAX-120;
            e.Graphics.DrawImage(DeadBat, new Point(HorizontalPosition, heartTop)); //show a dead-bat icon
            HorizontalPosition += DeadBat.Width + 5;


            int rest = Player.KilledEnemies;
            do //parse the integer into digit and display them as images, do this at least once -> each integer consists of at least one digit 
            {
                int SheetPosition = 0;
                if (rest < 10) //last digit to parse
                {
                    SheetPosition = rest; 
                    rest = 0; //end condition for do-while loop
                }
                else
                {
                    SheetPosition = rest / 10; //get the first digit of the number
                    rest = rest % 10; //only keep the rest 
                }
                // IMPORTANT: only parses two digit numbers for now (space is limited anyway)
                        
                e.Graphics.DrawImage(Numbers, new Rectangle(HorizontalPosition,heartTop ,NumberDimension*2, NumberDimension*2), new Rectangle(SheetPosition * NumberDimension, 0, NumberDimension, NumberDimension), GraphicsUnit.Pixel);
                HorizontalPosition += 6 + NumberDimension; //add some free space between numbers
            }
            while (rest > 0);
        }

        /// <summary>
        /// Turns the currently pressed direction keys into a Direction enum value
        /// </summary>
        private void MoveInKeyDirection()
        {
            if (up && right) { Player.Move(Direction.NorthEast); return; }
            if (down && right) { Player.Move(Direction.SouthEast); ; return; }
            if (up && left) { Player.Move(Direction.NorthWest);  return; }
            if (down && left) { Player.Move(Direction.SouthWest);  return; }
            if (up) { Player.Move(Direction.North);  return; }
            if (down) { Player.Move(Direction.South);  return; }
            if (right) { Player.Move(Direction.East);  return; }
            if (left) { Player.Move(Direction.West);  return; }
        }
   }
}
