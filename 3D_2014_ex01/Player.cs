﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _3D_2014_ex01
{
    /// <summary>
    /// Represents a character that is controlled by a player. 
    /// </summary>
    class Player : CharacterSprite
    {

        private HashSet<Projectile> ActiveProjectiles = new HashSet<Projectile>(); //current projectiles
        public Image Projectile { get; set; }
        
        private int OuterWindowBound = -10;  //use this value instead of 0 when checking if the player walks out of the window -> smoother transition 
                                             //from one end of the window to the other
        
        public int KilledEnemies { get; set; } 

        public Player(Image sheet, int width, int height)
        {
            SpriteSheet = sheet;
            SpriteWidth = width;
            SpriteHeight = height;
            ViewDirection = Direction.South;
            SpriteCursor = 0;
            SpriteSheetWidth = SpriteSheet.Width - SpriteWidth;
            KilledEnemies = 0;
        }

        /// <summary>
        /// Moves the character based on the player's input into the given direction with the specified stepsize
        /// </summary>
        /// <param name="dir">execute move into this direction</param>
        public override void Move(Direction dir)
        {
            ViewDirection = dir; //set new view direction
            if (SpriteCursor < SpriteSheetWidth) { SpriteCursor += SpriteWidth; } //take the next spritecolumn in the spritesheet
            else { SpriteCursor = 0; }

            switch (ViewDirection) //move based on the given direction
                    //if the character walks out of the window on one side it will walk back into it on the opposite side
            {
                case Direction.North:
                    if (CurrentPositionY < OuterWindowBound) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    break;
                case Direction.East:
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = OuterWindowBound; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.South:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = OuterWindowBound; } else { CurrentPositionY += StepSize; }
                    break;
                case Direction.West:
                    if (CurrentPositionX < OuterWindowBound) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                case Direction.NorthEast:
                    if (CurrentPositionY < OuterWindowBound) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = OuterWindowBound; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.SouthEast:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = OuterWindowBound; } else { CurrentPositionY += StepSize; }
                    if (CurrentPositionX > HorizontalBound) { CurrentPositionX = OuterWindowBound; } else { CurrentPositionX += StepSize; }
                    break;
                case Direction.SouthWest:
                    if (CurrentPositionY > VerticalBound) { CurrentPositionY = OuterWindowBound; } else { CurrentPositionY += StepSize; }
                    if (CurrentPositionX < OuterWindowBound) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                case Direction.NorthWest:
                    if (CurrentPositionY < OuterWindowBound) { CurrentPositionY = VerticalBound; } else { CurrentPositionY -= StepSize; }
                    if (CurrentPositionX < OuterWindowBound) { CurrentPositionX = HorizontalBound; } else { CurrentPositionX -= StepSize; }
                    break;
                default: break;
            }
        }

        /// <summary>
        /// Checks if the character collides with the given CharacterSprite through a simple four-point(corner) bounding box
        /// </summary>
        /// <param name="c">CharacterSprite to check for collisions</param>
        public override void IsCollidingWith(CharacterSprite c)
        {
            CheckForProjectileCollision(c); //check if any projectile hits the CharacterSprite
            if (!c.IsStillAlive()) //some bullet already killed this character
            {
                KilledEnemies++;
                return;
            }

            //simple four point collision detection (two x-values and two y-values per sprite)
           
            //(x1/y1)----------(x2/y1)
            // |...................|
            // |...................|
            // |.......Sprite......|
            // |...................|
            // |...................|
            //(x1/y2)----------(x2/y2)

            //player points:
            int PX1 = CurrentPositionX;
            int PY1 = CurrentPositionY;
            int PX2 = CurrentPositionX + SpriteWidth;
            int PY2 = CurrentPositionY + SpriteHeight; 

            //characterSprite points:
            int CX1 = c.CurrentPositionX;
            int CY1 = c.CurrentPositionY;
            int CX2 = c.CurrentPositionX + c.SpriteWidth;
            int CY2 = c.CurrentPositionY + c.SpriteHeight;

            if ((CX1 >= PX1 && CX1 <= PX2) && (CY1 >= PY1 && CY1 <= PY2)) //top left corner of the character sprite is colliding with the player
            {
                this.Collide();
                c.Collide();
                if (!c.IsStillAlive()) KilledEnemies++;
                return;
            }
            if ((CX2 >= PX1 && CX2 <= PX2) && (CY1 >= PY1 && CY1 <= PY2)) //top right corner
            {
                this.Collide();
                c.Collide();
                if (!c.IsStillAlive()) KilledEnemies++;
                return ;
            }
            if ((CX1 >= PX1 && CX1 <= PX2) && (CY2 >= PY1 && CY2 <= PY2)) //bottom left
            {
                this.Collide();
                c.Collide();
                if (!c.IsStillAlive()) KilledEnemies++;
                return ;
            }
            if ((CX2 >= PX1 && CX2 <= PX2) && (CY2 >= PY1 && CY2 <= PY2)) //bottom right corner
            {
                this.Collide();
                c.Collide();
                if (!c.IsStillAlive()) KilledEnemies++;
                return ;
            }
        }

        /// <summary>
        /// Check if any projectile hits the given CharacterSprite
        /// </summary>
        /// <param name="c">CharacterSprite to check for collisions</param>
        private void CheckForProjectileCollision(CharacterSprite c)
        {
            foreach (Projectile pro in ActiveProjectiles)
            {
                pro.IsCollidingWith(c);
            }
        }

        /// <summary>
        /// Reduces the player's lifepoints by 1
        /// Does not check for collision ! 
        /// </summary>
        public override void Collide()
        {
            LifePoints--;
        }

        /// <summary>
        /// Executes a single shot, i.e. createing a new Projectile that flies in the Player's current view direction
        /// </summary>
        public void shoot()
        {
            int x = 0; //try to find x and y start positions for the projectile that more or less fit the barrel position in the sprite
            int y = 0;
            switch (ViewDirection)
            {
                case Direction.North:
                    x = CurrentPositionX + (SpriteWidth / 2);
                    y = CurrentPositionY;
                    break;
                case Direction.East:
                    x = CurrentPositionX + SpriteWidth;
                    y = CurrentPositionY + (SpriteHeight/2);
                    break;
                case Direction.South:
                    x = CurrentPositionX + (SpriteWidth / 2);
                    y = CurrentPositionY + SpriteHeight;
                    break;
                case Direction.West:
                    x = CurrentPositionX;
                    y = CurrentPositionY + (SpriteHeight / 2);
                    break;
                case Direction.NorthEast:
                    x = CurrentPositionX + SpriteWidth;
                    y = CurrentPositionY;
                    break;
                case Direction.SouthEast:
                    x = CurrentPositionX + SpriteWidth;
                    y = CurrentPositionY + SpriteHeight;
                    break;
                case Direction.SouthWest:
                    x = CurrentPositionX;
                    y = CurrentPositionY + SpriteHeight;
                    break;
                case Direction.NorthWest:
                    x = CurrentPositionX;
                    y = CurrentPositionY;
                    break;
                default: break;
            }

            //create the new projectile and add it to the projectile set 
             ActiveProjectiles.Add(new Projectile(Projectile, Projectile.Height, Projectile.Height, this.ViewDirection, this)
            {
                VerticalBound = this.VerticalBound,
                HorizontalBound = this.HorizontalBound,
                CurrentPositionX = x,
                CurrentPositionY = y
            });
            
        }

        /// <summary>
        /// Display-position and size of the CharacterSprite
        /// </summary>
        /// <returns>Rectangle that contains information about display-position and size</returns>
        public override Rectangle GetDestinationRectangle()
        {
            return new Rectangle(CurrentPositionX, CurrentPositionY, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Which part of the spritesheet should be displayed
        /// </summary>
        /// <returns>Rectangle that contains information about position and size of single sprite in spritesheet</returns>
        public override Rectangle GetSourceSelectionRectangle()
        {
            return new Rectangle(SpriteCursor, (int)ViewDirection * SpriteHeight, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Returns the set of currently active projectiles
        /// </summary>
        /// <returns>Set of Projectiles</returns>
        public HashSet<Projectile> GetProjectiles()
        {
            return ActiveProjectiles;
        }

        /// <summary>
        /// Move all of the player's projectiles
        /// </summary>
        public void MoveProjectiles()
        {
            ActiveProjectiles.RemoveWhere(p => !p.IsStillAlive()); //remove all "dead" projectiles
            foreach (Projectile pro in ActiveProjectiles)
            {  //move all remaining
                pro.Move(Direction.East); //Direction is not taken into account in projectile move, since it doesn't change its' original direction
            }
        }
    }
}
