﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _3D_2014_ex01
{
    class Projectile : CharacterSprite
    {
        private CharacterSprite Parent; // i.e. shooter --> projectile's attackpoints == parents current attackpoints 

         public Projectile(Image sheet, int width, int height, Direction view, CharacterSprite parent)
        {
            SpriteSheet = sheet;
            SpriteWidth = width;
            SpriteHeight = height;
            ViewDirection = view;
            SpriteCursor = 0;
            SpriteSheetWidth = SpriteSheet.Width - SpriteWidth;
            LifePoints = 10;
            StepSize = 10;
            Parent = parent;
        }
      

        /// <summary>
        /// Reduces the lifepoints to 0 (-> projectile is remove from the screen)
        /// Does not check for collision ! 
        /// </summary>
        public override void Collide()
        {
            LifePoints = 0; 
        }

        /// <summary>
        /// Display-position and size of the CharacterSprite
        /// </summary>
        /// <returns>Rectangle that contains information about display-position and size</returns>
        public override Rectangle GetDestinationRectangle()
        {
            return new Rectangle(CurrentPositionX, CurrentPositionY, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Which part of the spritesheet should be displayed
        /// </summary>
        /// <returns>Rectangle that contains information about position and size of single sprite in spritesheet</returns>
        public override Rectangle GetSourceSelectionRectangle()
        {
            return new Rectangle((int)ViewDirection * SpriteWidth, 0, SpriteWidth, SpriteHeight);
        }

        /// <summary>
        /// Moves the projetile in the direction that was set during creation
        /// </summary>
        /// <param name="dir">has no effect</param>
        public override void Move(Direction dir)
        {              
            switch (ViewDirection)
            {
                case Direction.North: //ignore projectiles "leaving" the window (don't let it reappear on the opposite side of the window)
                    CurrentPositionY -= StepSize; 
                    break;
                case Direction.East:
                    CurrentPositionX += StepSize; 
                    break;
                case Direction.South:
                     CurrentPositionY += StepSize; 
                    break;
                case Direction.West:
                     CurrentPositionX -= StepSize; 
                    break;
                case Direction.NorthEast:
                    CurrentPositionY -= StepSize; CurrentPositionX += StepSize; 
                    break;
                case Direction.SouthEast:
                    CurrentPositionY += StepSize; CurrentPositionX += StepSize; 
                    break;
                case Direction.SouthWest:
                    CurrentPositionY += StepSize;  CurrentPositionX -= StepSize; 
                    break;
                case Direction.NorthWest:
                   CurrentPositionY -= StepSize;  CurrentPositionX -= StepSize;
                    break;
                default: break;
            }

            //decrease lifepoints by 1 per step -> works as a distance counter
            //the game constantly checks for the lifepoints of the sprites on screen, therefore as soon as the projectile has move 10 times it will be removed automatically
            LifePoints--;
        }

        /// <summary>
        /// Checks if the projectile collides with the given CharacterSprite through a simple four-point(corner) bounding box
        /// </summary>
        /// <param name="c">CharacterSprite to check for collisions</param>
        public override void IsCollidingWith(CharacterSprite c)
        {
            //simple four point collision detection (two x-values and two y-values per sprite)

            //(x1/y1)----------(x2/y1)
            // |...................|
            // |...................|
            // |.......Sprite......|
            // |...................|
            // |...................|
            //(x1/y2)----------(x2/y2)

            //projectile points:
            int PX1 = CurrentPositionX;
            int PY1 = CurrentPositionY;
            int PX2 = CurrentPositionX + SpriteWidth;
            int PY2 = CurrentPositionY + SpriteHeight;

            //characterSprite points:
            int CX1 = c.CurrentPositionX;
            int CY1 = c.CurrentPositionY;
            int CX2 = c.CurrentPositionX + c.SpriteWidth;
            int CY2 = c.CurrentPositionY + c.SpriteHeight;

            //since the projectile sprite is smaller than the enemy check if the projectile corners lie "inside" the enemy outline

            if ((PX1 >= CX1 && PX1 <= CX2) && (PY1 >= CY1 && PY1 <= CY2)) //top left corner of the character sprite is colliding with the player
            {
                 this.Collide();
                c.GotHit(Parent.AttackPoints);
                return;
            }
            if ((PX2 >= CX1 && PX2 <= CX2) && (PY1 >= CY1 && PY1 <= CY2)) //top right corner
            {
                this.Collide();
                c.GotHit(Parent.AttackPoints);
                return;
            }
            if ((PX1 >= CX1 && PX1 <= CX2) && (PY2 >= CY1 && PY2 <= CY2)) //bottom left
            {
                this.Collide();
                c.GotHit(Parent.AttackPoints);
                return;
            }
            if ((PX2 >= CX1 && PX2 <= CX2) && (PY2 >= CY1 && PY2 <= CY2)) //bottom right corner
            {
                this.Collide();
                c.GotHit(Parent.AttackPoints);
                return;
            }
        }
    }
}
