﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _3D_2014_ex01
{
    /// <summary>
    /// Abstract class that contains basic fields/methods a sprite that represents a character (player or non-player) might need
    /// </summary>
    abstract class CharacterSprite
    {
        //sprite-related fields:
        public Image SpriteSheet { get; set; } 
        public int SpriteWidth { get; set; }
        public int SpriteHeight { get; set; }
        public int SpriteSheetWidth { get; set; }
        protected int SpriteCursor { get; set; } //horizontal position the current sprite starts at

        //character-related fields:
        public int LifePoints { get; set; }
        public int AttackPoints { get; set; }
        public int StepSize { get; set; }
        public Direction ViewDirection { get; set; } // = walk direction
        public int CurrentPositionX { get; set; }
        public int CurrentPositionY { get; set; }

        //max. bounds of the space the character can walk in 
        public int VerticalBound { get; set; }
        public int HorizontalBound { get; set; }


        /// <summary>
        /// Moves the CharacterSprite
        /// </summary>
        /// <param name="dir">execute move into this direction</param>
        public abstract void Move(Direction dir);

        /// <summary>
        /// Handles what happens if the CharacterSprite collides with something. 
        /// Does not check for collision ! 
        /// </summary>
        public abstract void Collide();

        /// <summary>
        /// Checks if the current CharacterSprite collides with the given CharacterSprite through a simple four-point(corner) bounding box
        /// </summary>
        /// <param name="c">CharacterSprite to check for collisions</param>
        public abstract void IsCollidingWith(CharacterSprite c);

        /// <summary>
        /// Display-position and size of the CharacterSprite
        /// </summary>
        /// <returns>Rectangle that contains information about display-position and size</returns>
        public abstract Rectangle GetDestinationRectangle();
       
        /// <summary>
        /// Which part of the spritesheet should be displayed
        /// </summary>
        /// <returns>Rectangle that contains information about position and size of single sprite in spritesheet</returns>
        public abstract Rectangle GetSourceSelectionRectangle();

        /// <summary>
        /// Handles getting hit by an attacking instance, e.g. a projectile
        /// </summary>
        /// <param name="attack">attacking instance's attack points</param>
        /// <returns>CharacterSprite is alive(true)/dead(false)</returns>
        public bool GotHit(int attack)
        {
            LifePoints -= attack;
            return (LifePoints <= 0) ? false : true;
        }

        /// <summary>
        /// Checks if CharacterSprite is still alive
        /// </summary>
        /// <returns>is alive(true)/dead(false)</returns>
        public bool IsStillAlive()
        {
            return  (LifePoints <= 0) ? false : true;
        }

       
    }
}
